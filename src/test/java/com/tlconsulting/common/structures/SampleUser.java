package com.tlconsulting.common.structures;

import com.tlconsulting.common.serializer.Serializer;

public class SampleUser extends Serializer {
	/**
	 * 
	 */
	public String username;
	public String password;
	
	@SuppressWarnings("unused")
	private SampleUser() {
        super();
    }
	
	public SampleUser(String user, String password)
	{
		this.username = user;
		this.password = password;
	}
}
