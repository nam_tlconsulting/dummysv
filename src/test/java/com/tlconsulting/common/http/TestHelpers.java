package com.tlconsulting.common.http;

import java.io.DataInputStream;
import java.io.EOFException;
import java.io.IOException;
import java.io.InputStream;
import java.net.Socket;

import org.apache.http.HttpResponse;

public class TestHelpers
{
	public static boolean isLocalPortAvailable(int port)
	{
		Socket s = null;
		try
		{
			s = new Socket("127.0.0.1", port);
			return false;
		}
		catch (Exception e)
		{
			return true;
		}
		finally
		{
			if(s != null)
				try {s.close();}
			catch(Exception e){}
		}
	}
	
	public static boolean matchResponses(HttpResponse source, HttpResponse target) throws UnsupportedOperationException, IOException
	{
		return isEqual(source.getEntity().getContent(),target.getEntity().getContent());
	}
	
	private static boolean isEqual(InputStream i1, InputStream i2) throws IOException {
		byte[] buf1 = new byte[64 *1024];
	    byte[] buf2 = new byte[64 *1024];
	    try {
	        DataInputStream d2 = new DataInputStream(i2);
	        int len;
	        while ((len = i1.read(buf1)) > 0) {
	            d2.readFully(buf2,0,len);
	            for(int i=0;i<len;i++)
	              if(buf1[i] != buf2[i]) {
	            	  d2.close();
	            	  return false;
	              }
	        }
	        return d2.read() < 0;
	    } catch(EOFException ioe) {
	        return false;
	    } finally {
	        i1.close();
	        i2.close();
	    }
	}
}
