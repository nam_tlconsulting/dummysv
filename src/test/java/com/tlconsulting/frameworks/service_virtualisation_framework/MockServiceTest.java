package com.tlconsulting.frameworks.service_virtualisation_framework;

import static com.github.tomakehurst.wiremock.http.RequestMethod.*;
import static com.tlconsulting.common.http.TestHelpers.*;
import static com.tlconsulting.common.comparer.MatchOperation.*;
import static com.tlconsulting.common.files.FileOperations.*;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.io.IOException;
import java.net.URISyntaxException;
import java.util.UUID;

import org.apache.http.HttpResponse;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.tlconsulting.common.http.HttpClient;
import com.tlconsulting.common.structures.SampleUser;
import com.tlconsulting.common.serializer.Serializer;

public class MockServiceTest {

	private static final int LOCAL_SERVICE_PORT = 8888;
	private static final int REMOTE_SERVICE_PORT = 80;
	private static final String LOCAL_HOST = "localhost";
	private static final String REMOTE_HOST = "jsonplaceholder.typicode.com";
	private static final String REMOTE_NAME = "jsonplaceholder";
	private static final String ENDPOINT = "/users";
	private static final String ALBUMS_ENDPOINT = "/albums";

	MockService mockService;
	HttpClient localClient;
	HttpClient remoteClient;

	@Before
	public void init() {
		ServiceParameters params = ServiceParameters.Builder.newInstance(REMOTE_HOST, REMOTE_NAME)
															.setUpstreamPort(REMOTE_SERVICE_PORT)
															.setPort(LOCAL_SERVICE_PORT)
															.setHttps(false)
															.build();
		mockService = new MockService(params);
		mockService.startService();
		localClient = new HttpClient(LOCAL_HOST, LOCAL_SERVICE_PORT);
		remoteClient = new HttpClient(REMOTE_HOST, REMOTE_SERVICE_PORT);
	}

	@Test
	public void testStartStopService() {
		int port = 3333;
		ServiceParameters params = ServiceParameters.Builder.newInstance(REMOTE_HOST, REMOTE_NAME)
															.setUpstreamPort(REMOTE_SERVICE_PORT)
															.setPort(port)
															.setHttps(false)
															.build();
		MockService ms = new MockService(params);
		ms.startService();
		assertFalse(isLocalPortAvailable(port));
		ms.stopService();
		assertTrue(isLocalPortAvailable(port));
	}

	@Test
	public void testRecord() throws Exception {

		// Add endpoint to mockService
		EndPoint endpoint = mockService.addEndPoint(UUID.randomUUID(), "com.typicode.jsonplaceholder.users",
				Mode.RECORD, ENDPOINT);
		endpoint.record();

		HttpResponse localResponse = localClient.get(ENDPOINT + "?id=1");
		HttpResponse remoteResponse = remoteClient.get(ENDPOINT + "?id=1");
		assertTrue(matchResponses(localResponse, remoteResponse));

		// Enable Playback
		remoteResponse = remoteClient.get(ENDPOINT + "?id=1");
		endpoint.playback();
		HttpResponse stubbedResponse = localClient.get(ENDPOINT + "?id=1");
		assertTrue(matchResponses(stubbedResponse, remoteResponse));
	}

	@Test
	public void testProxy() throws Exception {

		EndPoint endpoint = mockService.addEndPoint(UUID.randomUUID(), "com.typicode.jsonplaceholder.users", Mode.PROXY,
				ENDPOINT);
		endpoint.proxy();

		// Read the response from local and upstream servers
		HttpResponse localResponse = localClient.get(ENDPOINT);
		HttpResponse remoteResponse = remoteClient.get(ENDPOINT);

		assertTrue(matchResponses(localResponse, remoteResponse));
	}

	@Test
	public void testHybrid() throws Exception {
		mockService.removeEndPointByName("com.typicode.jsonplaceholder.albums");
		EndPoint endpoint = mockService.addEndPoint(UUID.randomUUID(), "com.typicode.jsonplaceholder.albums",
				Mode.HYBRID, ALBUMS_ENDPOINT);

		HttpResponse remoteResponse = remoteClient.get(ALBUMS_ENDPOINT + "/9");
		
		// Enable hybrid
		endpoint.hybrid();
		HttpResponse localResponse = localClient.get(ALBUMS_ENDPOINT + "/9");
		assertTrue(matchResponses(localResponse, remoteResponse));

		// Set playback() or leave it
		// it will automatically get response from local.
		remoteResponse = remoteClient.get(ALBUMS_ENDPOINT + "/9");
		HttpResponse stubbedResponse = localClient.get(ALBUMS_ENDPOINT + "/9");
		assertTrue(matchResponses(stubbedResponse, remoteResponse));
	}

	@Test
	public void testCreateScenario() {

		Request request = new Request.Builder("/comments", GET)
				.addHeader("Accept", contains, "application/json")
				.addQueryParameter("postId", equalTo, "1")
				.build();
		
		Response response = new Response.Builder(200).addBody("this is a body").addHeader("Accept", "application/json").build();

		EndPoint endPoint = mockService.addEndPoint(UUID.randomUUID(), "com.typicode.jsonplaceholder.comments",
				Mode.PLAYBACK, "/comments");

		Scenario scenario = endPoint.createScenario(request, response);
		// check fields in request and response match to the new scenario
		assert (scenario.request.getRequestMethod().equals(request.getRequestMethod()));
		assert (scenario.response.getBody().equals(response.getBody()));
		assert (scenario.response.getStatusCode() == response.getStatusCode());
	}

	@Test
	public void testWriteData() {
		String filename = "temp.json";
		String stringData = "{\"name\":\"John\"}";
		writeTextFile(stringData, filename);

		// Open file and read to verify
		String outputString = readTextFile(filename);
		assertEquals(stringData, outputString);
	}

	@Test
	public void testReadData() {
		String filename = "temp.json";
		String expectedString = "{\"name\":\"John\"}";
		String output = readTextFile(filename);
		assertEquals(output, expectedString);
	}

	@Test
	public void testSerializeJson() {

		/**
		 * User object { "username": "user", "password": "123" }
		 */
		// function: public String serialise(Object o)
		SampleUser user = new SampleUser("user", "123");
		String jsonString = user.toJSON();

		assert (jsonString.contains("username"));
		assert (jsonString.contains("user"));
		assert (jsonString.contains("password"));
		assert (jsonString.contains("123"));
	}

	@Test
	public void testDeserializeJson() {
		// Create a generic class of Response<T> and a field private T body;
		String jsonString = "{\r\n" + "        \"username\": \"1\",\r\n" + "        \"password\": \"John\"\r\n"
				+ "    }";

		SampleUser user = Serializer.jsonToObject(jsonString, SampleUser.class);
		assert (user.username.equals("1"));
		assert (user.password.equals("John"));
	}

	@Test
	public void testSerializeXML() {

		/**
		 * User object { "username": "user", "password": "123" }
		 */
		// function: public String serialise(Object o)
		SampleUser user = new SampleUser("user", "123");
		String XMLString = user.toXML();

		assert (XMLString.contains("username"));
		assert (XMLString.contains("user"));
		assert (XMLString.contains("password"));
		assert (XMLString.contains("123"));
	}

	@Test
	public void testDeserializeXML() {
		// Create a generic class of Response<T> and a field private T body;
		String XMLString = "<SampleUser>\r\n" + "<username>JohnDoe</username>\r\n" + "<password>jd123</password>\r\n"
				+ "</SampleUser>";

		SampleUser user = Serializer.xmlToObject(XMLString, SampleUser.class);

		assert (user.username.equals("JohnDoe"));
		assert (user.password.equals("jd123"));
	}

	@Test
	public void testUndefined() throws IOException, URISyntaxException {
		HttpResponse localResponse = localClient.get("/nothing/overthere?id=1&name=josh");
		assertTrue(localResponse.getStatusLine().getStatusCode() == 404);
	}

	@After
	public void tearDown() {
		mockService.stopService();
	}
}
