package com.tlconsulting.frameworks.service_virtualisation_framework;

public enum Mode {
	RECORD,
	PROXY,
	HYBRID,
	PLAYBACK,
	CUSTOM,
	UNDEFINED
}
