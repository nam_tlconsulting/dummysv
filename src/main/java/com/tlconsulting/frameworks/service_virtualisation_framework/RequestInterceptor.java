package com.tlconsulting.frameworks.service_virtualisation_framework;

import com.github.tomakehurst.wiremock.common.Errors;
import com.github.tomakehurst.wiremock.extension.requestfilter.RequestFilterAction;
import com.github.tomakehurst.wiremock.extension.requestfilter.StubRequestFilter;
import com.github.tomakehurst.wiremock.http.HttpHeaders;
import com.github.tomakehurst.wiremock.http.RequestMethod;
import com.github.tomakehurst.wiremock.http.ResponseDefinition;
import com.github.tomakehurst.wiremock.recording.RecordSpecBuilder;

import static com.github.tomakehurst.wiremock.client.WireMock.*;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class RequestInterceptor extends StubRequestFilter{
	private static final Logger logger = LogManager.getLogger();

	private MockService mockService;
	private EndPoint endPoint;

	public void setMockService(MockService mockService) {
		this.mockService = mockService;
	}

	@Override
	public RequestFilterAction filter(com.github.tomakehurst.wiremock.http.Request wireMockRequest) {
		endPoint = mockService.getEndPointByUrl(wireMockRequest.getUrl());

		logRequest(wireMockRequest);

		switch(endPoint.getMode())
		{
			case RECORD:
				mockService.getServer().startRecording(new RecordSpecBuilder().forTarget(mockService.getAbsoluteUpstreamUrl()).ignoreRepeatRequests().build());
				logger.info("Action: FWD ["+ mockService.getAbsoluteUpstreamUrl() + wireMockRequest.getUrl() + "]");
				break;
			case PROXY:
				mockService.getServer().stubFor(proxyAllTo(mockService.getAbsoluteUpstreamUrl()).atPriority(1));
				logger.info("Action: FWD ["+ mockService.getAbsoluteUpstreamUrl() + wireMockRequest.getUrl() + "]");
				break;
			case HYBRID:
				boolean isMatched = mockService.isRequestMatchedWithAnyScenarios(endPoint, wireMockRequest);
				if(!isMatched) {
					mockService.getServer().startRecording(new RecordSpecBuilder().forTarget(mockService.getAbsoluteUpstreamUrl()).ignoreRepeatRequests().build());
					logger.info("Action: FWD ["+ mockService.getAbsoluteUpstreamUrl() + wireMockRequest.getUrl() + "]");
				}
				else
				{
					logger.info("Action: STUB [" + wireMockRequest.getAbsoluteUrl() + "]");
				}
				break;
			case PLAYBACK:
				logger.info("Action: STUB [" + wireMockRequest.getAbsoluteUrl() + "]");
				break;
			case CUSTOM:
				logger.info("Action: CUSTOM RESPONSE [" + wireMockRequest.getAbsoluteUrl() + "]");
				String customHandler = mockService.getCustomHandler();
				if(customHandler == "") {
					logger.error("Redirect URL is not configured !!");
					return RequestFilterAction.stopWith(ResponseDefinition.badRequest(Errors.notPermitted("Redirect URL is not configured")));
				}	
				else {
					logger.info("Action: FORWARD to custom handler " + customHandler);
					mockService.getServer().stubFor(proxyAllTo(customHandler).atPriority(1));
				}
				break;
			case UNDEFINED:
				logger.info("Action: UNDEFINED [" + wireMockRequest.getAbsoluteUrl() + "]");
				return RequestFilterAction.stopWith(ResponseDefinition.notConfigured());
		}

        return RequestFilterAction.continueWith(wireMockRequest);
	}

	@Override
	public String getName() {
		return "default-interceptor";
	}

	private void logRequest(com.github.tomakehurst.wiremock.http.Request request) {
		RequestMethod method = request.getMethod();
		String url = request.getAbsoluteUrl();
		HttpHeaders headers = request.getHeaders();
		String body = request.getBodyAsString();
		Mode mode = endPoint.getMode();

		logger.info(mode + " Request: INBOUND [" + method + " " + url + "]");
		logger.debug("\n" + headers + "\n" + body);
	}
}
