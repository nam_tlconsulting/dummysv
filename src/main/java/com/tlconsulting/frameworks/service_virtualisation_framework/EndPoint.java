package com.tlconsulting.frameworks.service_virtualisation_framework;

import static com.tlconsulting.common.files.FileOperations.*;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import com.github.tomakehurst.wiremock.stubbing.StubMapping;
import com.tlconsulting.common.serializer.*;

public class EndPoint extends Serializer implements Comparable<EndPoint> {
	private transient MockService parentMockService;
	private UUID id;
	private Mode mode = Mode.UNDEFINED;
	private String url;
	private String name;
	private List<String> scenarios;
	
	public EndPoint(UUID id, String name, Mode mode, String url, MockService mockService) {
		this.id = id;
		this.name = name;
		this.mode = mode;
		this.url = url;
		this.scenarios = new ArrayList<>();
		this.parentMockService = mockService;
		// Save endpoint to file
		saveToJson();
	}

	public void playback() {
		mode = Mode.PLAYBACK;
		saveToJson();
	}

	public void record() {
		mode = Mode.RECORD;
		saveToJson();
	}

	public void proxy() {
		mode = Mode.PROXY;
		saveToJson();
	}

	public void hybrid() {
		mode = Mode.HYBRID;
		saveToJson();
	}

	public void setName(String name) {
		this.name = name;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public void setMode(Mode mode) {
		switch (mode) {
		case PLAYBACK:
			playback();
			break;
		case RECORD:
			record();
			break;
		case PROXY:
			proxy();
			break;
		case HYBRID:
			hybrid();
			break;
		default:
			getMode();
		}
	}

	public Mode getMode() {
		return mode;
	}

	public UUID getId() {
		return id;
	}

	public String getName() {
		return name;
	}

	public String getUrl() {
		return this.url;
	}

	public List<String> getScenarios() {
		return scenarios;
	}

	public MockService getParentMockService() {
		return parentMockService;
	}
	
	public void setParentMockService(MockService parent) {
		this.parentMockService = parent;
	}
	
	public Scenario createScenario(Request request, Response response) {
		return createScenario(request, response, StubMapping.DEFAULT_PRIORITY);
	}

	public Scenario createScenario(Request request, Response response, int priority) {
		Scenario scenario = new Scenario(this, request, response, priority);
		this.addScenario(scenario.getName());
		return scenario;
	}
	
	public List<StubMapping> getStubMappings(MockService mockService, EndPoint endPoint) {
		List<StubMapping> mappings = new ArrayList<>();
		for (UUID scenario : getScenarioIds()) {
			mappings.add(mockService.getServer().getSingleStubMapping(scenario));
		}
		return mappings;
	}
	
	public List<UUID> getScenarioIds() {
		List<UUID> scenarioIds = new ArrayList<>();
		for(String scenario: this.scenarios) {
			scenario = scenario.substring(scenario.indexOf('-'), scenario.lastIndexOf('.'));
			scenario = scenario.replaceFirst("-", "");
			scenarioIds.add(UUID.fromString(scenario));
		}
		return scenarioIds;
	}
	
	@Override
    public int compareTo(EndPoint e) {
        return this.getMode().compareTo(e.getMode());
    }

	void addScenario(String scenarioName) {
		scenarios.add(scenarioName);
		this.saveToJson();
	}
	
	private void saveToJson() {
		String text = this.toJSON();
		writeTextFile(text, parentMockService.getEndPointsFolder() + "/" + name + ".json");
	}
}
