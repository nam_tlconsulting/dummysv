package com.tlconsulting.frameworks.service_virtualisation_framework;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;
import java.util.Enumeration;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.tlconsulting.common.http.HttpsTrustManager;

public class MockServiceServletWrapper extends HttpServlet {

	private static final long serialVersionUID = 6321205249756755277L;
	private static final Logger logger = LogManager.getLogger();
	private MockService mockService;
	private static final String configurationFile = "/WEB-INF/classes/config.properties";
	private boolean ignoreSSL;

	public void init(ServletConfig config) throws ServletException {
		logger.info("START Startup script");
		super.init(config);
		String startupClassName = config.getInitParameter("startup");
		logger.debug("Startup class name: " + startupClassName);
		
		Properties prop = new Properties();
		boolean watcher = false;
		try (InputStream input = getServletContext().getResourceAsStream(configurationFile)) {
			logger.info("Loading configuration from file: " + configurationFile);
			prop.load(input);
			ignoreSSL = Boolean.parseBoolean(prop.getProperty("https.ignoressl", "false"));
			watcher = Boolean.parseBoolean(prop.getProperty("watcher.enabled", "false"));
			
			if (ignoreSSL)
				logger.warn("SSL header verification is DISABLED.");
		} catch (IOException e) {
			logger.error(e.getMessage(), e);
		}
		
		try {
			MockServiceImpl startupObject = (MockServiceImpl) Class.forName(startupClassName).newInstance();
			logger.debug("Initialisation class: " + startupObject.toString());
			Method initMethod = startupObject.getClass().getDeclaredMethod("init");
			logger.debug("Executing method: " + initMethod.getName());
			prop.setProperty("upstream.name", getServletName());
			logger.debug("Servlet name: {}", getServletName());
			MockService.setProperties(prop);
			logger.debug("UPSTREAM PROP: " + prop.get("upstream.host"));
			mockService = (MockService) initMethod.invoke(startupObject, new Object[] {});
			
			if(watcher)
				mockService.enableWatcher();
			
			logger.debug("UPSTREAM SET TO: " + mockService.getServiceParameters().getUpstream().getUpstreamUrl());
		} catch (IllegalAccessException | IllegalArgumentException | InvocationTargetException | InstantiationException
				| ClassNotFoundException | NoSuchMethodException | SecurityException e) {
			logger.error(e.getMessage(), e);
		}

		logger.info("STOP Initialisation class");
		logger.info("WAITING for connections..");
	}

	public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		forwardRequestToWireMock(request, response);
	}

	public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		forwardRequestToWireMock(request, response);
	}

	public void doPut(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		forwardRequestToWireMock(request, response);
	}

	public void doDelete(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		forwardRequestToWireMock(request, response);
	}

	public void destroy() {
		mockService.stopService();
		logger.info("STOP all services. Terminating.");
	}

	private void forwardRequestToWireMock(HttpServletRequest request, HttpServletResponse response) {
		try {
			String endpoint = request.getRequestURI().substring(request.getContextPath().length());
			
			String customHandler = request.getScheme() + "://"
			+ request.getLocalName()
			+ ":" + request.getLocalPort()
			+ request.getContextPath();
			mockService.setCustomHandlerHost(customHandler);
			endpoint = request.getQueryString() != null ? endpoint + "?" + request.getQueryString() : endpoint;
			logger.debug("PASSTHROUGH endpoint to wiremock: [" + request.getMethod() + " " + endpoint + "]");
			logger.debug("Custom Handler Host is set to: [" + customHandler + "]");

			if (ignoreSSL)
				HttpsTrustManager.allowAllSSL();

			HttpURLConnection.setFollowRedirects(false);
			URLConnection connection = (URLConnection) new URL(
					request.getScheme() + "://" + mockService.getServiceParameters().getHost() + ":" + mockService.getPort() + endpoint)
							.openConnection();
			((HttpURLConnection) connection).setRequestMethod(request.getMethod());
			copyHeadersToConnection(request, connection);

			if (!request.getMethod().equals("GET")) {
				connection.setDoOutput(true);
				InputStream input1 = request.getInputStream();
				OutputStream output1 = connection.getOutputStream();
				copyStream(input1, output1);
			}

			int responseCode = ((HttpURLConnection) connection).getResponseCode();

			response.setStatus(responseCode);

			copyHeadersToResponse(connection, response);
			InputStream input2 = null;
			OutputStream output2 = response.getOutputStream();

			try {
				input2 = connection.getInputStream();
			} catch (IOException e) {
				input2 = ((HttpURLConnection) connection).getErrorStream();
			}

			if (input2 != null) {
				copyStream(input2, output2);
			} else {
				response.sendError(500,
						"This is a service virtualisation framework generated error. Received null response from the server.");
			}
		} catch (IOException e) {
			logger.error(e.getMessage(), e);
				try {
					response.sendError(500, e.getMessage() + e.getStackTrace());
				} catch (IOException e1) {
					logger.error(e1.getMessage(), e1);
				}
		}
	}

	private void copyHeadersToConnection(HttpServletRequest request, URLConnection connection) {
		HttpServletRequest httpRequest = (HttpServletRequest) request;
		Enumeration<String> headerNames = httpRequest.getHeaderNames();
		if (headerNames != null) {
			logger.debug("copy request headers");
			while (headerNames.hasMoreElements()) {
				String nextHeaderName = headerNames.nextElement();
				String headerValue = httpRequest.getHeader(nextHeaderName);
				if (nextHeaderName.toLowerCase().contentEquals("accept-encoding"))
					continue;
				connection.setRequestProperty(nextHeaderName, headerValue);
				logger.debug(nextHeaderName + ": " + headerValue);
			}
		}
	}

	private void copyHeadersToResponse(URLConnection connection, HttpServletResponse response) {
		logger.debug("copy response headers");
		for (Map.Entry<String, List<String>> entry : connection.getHeaderFields().entrySet()) {
			String headerName = entry.getKey();
			String headerValue = String.join(",", entry.getValue());
			response.setHeader(headerName, headerValue);
			logger.debug(headerName + ": " + headerValue);
		}
	}

	private void copyStream(InputStream in, OutputStream out) throws IOException {
		byte[] buffer = new byte[1024];
		String str = "";
		logger.debug("BODY START");
		while (true) {
			int bytesRead = in.read(buffer);
			if (bytesRead == -1) {
				logger.debug(str);
				break;
			}
			str += new String(buffer);
			out.write(buffer, 0, bytesRead);
		}
		logger.debug("BODY END");

		try {
			out.flush();
			out.close();
		} catch (IOException e) {
			logger.error(e.getMessage(), e);
		}
	}
}
