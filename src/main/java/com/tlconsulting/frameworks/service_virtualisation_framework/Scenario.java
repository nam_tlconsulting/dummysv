package com.tlconsulting.frameworks.service_virtualisation_framework;

import java.util.UUID;

import com.github.tomakehurst.wiremock.client.MappingBuilder;
import com.github.tomakehurst.wiremock.stubbing.StubMapping;

public class Scenario {
	
	/**
	 * 
	 */
	public EndPoint parentEndPoint;
	public Request request;
	public Response response;
	private MappingBuilder ScenarioMapping;
	private StubMapping stub;
	UUID id;
	private String name;
	private int priority;
	
	public Scenario(EndPoint endPoint, Request request, Response response) {
		this(endPoint, request, response, StubMapping.DEFAULT_PRIORITY);
	}
	
	public Scenario(EndPoint endPoint, Request request, Response response, int priority) {
		this.parentEndPoint = endPoint;
		this.request = request;
		this.response = response;
		this.ScenarioMapping = this.request.getRequestMapping()
				.willReturn(this.response.getResponseMapping()).persistent();
		this.priority = priority;
		this.stub = endPoint.getParentMockService().getServer().stubFor(ScenarioMapping.atPriority(this.priority));
		this.id = stub.getId();
		this.stub.setName((request.getUrl() + "-" + id).replace("/", ""));
		endPoint.getParentMockService().updateStub(stub);
		this.name = stub.getName();
	}
	
	public MappingBuilder getScenarioMapping() {
		return this.ScenarioMapping;
	}
	
	public UUID getId() {
		return id;
	}

	public String getName() {
		return name;
	}
	
	public int getPriority() {
		return priority;
	}
	
	public EndPoint getParentEndPoint() {
		return parentEndPoint;
	}
	
	public void setPriority(int newPriority) {
		this.priority = newPriority;
		stub.setPriority(priority);
		this.getParentEndPoint().getParentMockService().updateStub(stub);
	}
}