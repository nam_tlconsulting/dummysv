package com.tlconsulting.frameworks.service_virtualisation_framework;

public class Upstream {
	private String upstreamHost;
	private int upstreamPort;
	private String upstreamName;
	private static final int DEFAULT_HTTP_PORT = 80;
	
	public Upstream(String host, String name) {
		this(host, DEFAULT_HTTP_PORT, name);
	}
	
	public Upstream(String host, int port, String name) {
		this.upstreamHost = host;
		this.upstreamPort = port;
		this.upstreamName = name;
	}
	
	public String getHost() {
		return upstreamHost;
	}

	public int getPort() {
		return upstreamPort;
	}
	
	public String getName() {
		return upstreamName;
	}
	
	public String getUpstreamUrl() {
		return upstreamHost + ":" + upstreamPort;
	}
	
	public void setUpstreamHost(String upstreamHost) {
		this.upstreamHost = upstreamHost;
	}

	public void setUpstreamPort(int upstreamPort) {
		this.upstreamPort = upstreamPort;
	}
}
