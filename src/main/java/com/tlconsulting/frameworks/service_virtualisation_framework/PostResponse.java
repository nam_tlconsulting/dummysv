package com.tlconsulting.frameworks.service_virtualisation_framework;

import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.github.tomakehurst.wiremock.core.Admin;
import com.github.tomakehurst.wiremock.extension.PostServeAction;
import com.github.tomakehurst.wiremock.http.HttpHeaders;
import com.github.tomakehurst.wiremock.http.LoggedResponse;
import com.github.tomakehurst.wiremock.http.RequestMethod;
import com.github.tomakehurst.wiremock.recording.RecordingStatus;
import com.github.tomakehurst.wiremock.recording.SnapshotRecordResult;
import com.github.tomakehurst.wiremock.stubbing.ServeEvent;
import com.github.tomakehurst.wiremock.stubbing.StubMapping;
import com.github.tomakehurst.wiremock.verification.LoggedRequest;
import static com.tlconsulting.common.files.FileOperations.renameFile;
import static com.tlconsulting.common.objects.ObjectUtils.setPrivateFinalField;;

public class PostResponse extends PostServeAction {
	private static final Logger logger = LogManager.getLogger();
	private MockService mockService;

	public void setMockService(MockService mockService) {
		this.mockService = mockService;
	}

	// TODO override doAction for each stub endpoint rather than globally
	@Override
	public void doGlobalAction(ServeEvent serveEvent, Admin admin) {
		EndPoint endPoint = mockService.getEndPointByUrl(serveEvent.getRequest().getUrl());
		logResponse(serveEvent, endPoint.getMode());
		if (mockService.getServer().getRecordingStatus().getStatus().equals(RecordingStatus.Recording)) {
			SnapshotRecordResult recordResult = mockService.getServer().stopRecording();
			List<StubMapping> mappings = recordResult.getStubMappings();
			
			// Add scenarios to endpoint
			for (StubMapping m : mappings) {
				if(mockService.getServiceParameters().isCaptureResponseTimes()) {
					try {
						setPrivateFinalField(m.getResponse(), "fixedDelayMilliseconds", serveEvent.getTiming().getProcessTime());
					} catch (SecurityException e) {
						logger.error("Error when modifying stub mapping fields (fixeddelay): {}", e);
					}
				}
				
				if(mockService.getServiceParameters().getChunkDribbleDelay() != null) {
					try {
						setPrivateFinalField(m.getResponse(), "chunkedDribbleDelay" , mockService.getServiceParameters().getChunkDribbleDelay());
					} catch (SecurityException e) {
						logger.error("Error when modifying stub mapping fields (chunkdribbledelay): {}", e);
					}
				}
				
				if(mockService.getServiceParameters().getDelayDistribution() != null) {
					try {
						setPrivateFinalField(m.getResponse(), "delayDistribution" , mockService.getServiceParameters().getDelayDistribution());
					} catch (SecurityException e) {
						logger.error("Error when modifying stub mapping fields (delaydistribution): {}", e);
					}
				}
				
				m.setPriority(StubMapping.DEFAULT_PRIORITY);
				
				String oldStubMappingNameFullPath = mockService.getMappingsFolder() + "/" + m.getName() + "-" + m.getUuid() + ".json";
				String newStubMappingNameFullPath = getNewStubMappingFileName(serveEvent, m);
				String newStubMappingName = newStubMappingNameFullPath.replace(mockService.getMappingsFolder() + "/", ""); 
				
				m.setName(newStubMappingName.replace(".json", ""));
				mockService.getServer().editStubMapping(m);
				
				renameFile(oldStubMappingNameFullPath, newStubMappingNameFullPath);
				endPoint.addScenario(newStubMappingName);
			}
		}
		if(endPoint.getMode() == Mode.PROXY)
		{
			StubMapping currentStub = serveEvent.getStubMapping();
			if(currentStub != null)
				mockService.getServer().removeStub(currentStub);
		}
	};

	@Override
	public String getName() {
		return "reset-playback-action";
	}

	private String getNewStubMappingFileName(ServeEvent serveEvent, StubMapping m) {
		String url = serveEvent.getRequest().getUrl();
		
		String path = mockService.removeUrlParams(url);
		String params = url.replace(path, "");
		path = path.replaceAll("/", "_");
		path = path.replaceAll("-", "_");
		path = path.replaceFirst("_", "/");
		params = params.replace("?", "");
		
		String newFileName =  (!params.equals("")) ? (mockService.getMappingsFolder() + path + "(" + params + ")-" + m.getUuid() +".json") 
														: (mockService.getMappingsFolder() + path + "-" + m.getUuid() + ".json");
		return newFileName;
	}
	


	private void logResponse(ServeEvent serveEvent, Mode mode) {
		LoggedRequest request = serveEvent.getRequest();
		LoggedResponse response = serveEvent.getResponse();
		RequestMethod method = request.getMethod();
		String url = request.getAbsoluteUrl();
		int status = response.getStatus();
		HttpHeaders headers = response.getHeaders();
		String body = response.getBodyAsString();

		logger.info(mode + " Response: OUTBOUND [" + method + " " + url + "] - STATUS " + status);
		logger.debug("\n" + headers + "\n" + body);
	}
}
