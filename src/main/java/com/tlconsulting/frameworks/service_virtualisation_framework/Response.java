package com.tlconsulting.frameworks.service_virtualisation_framework;

import java.util.HashMap;
import java.util.Map;

import com.github.tomakehurst.wiremock.client.ResponseDefinitionBuilder;
import com.github.tomakehurst.wiremock.http.ChunkedDribbleDelay;
import com.github.tomakehurst.wiremock.http.DelayDistribution;
import com.github.tomakehurst.wiremock.http.Fault;

import static com.github.tomakehurst.wiremock.client.WireMock.*;

public class Response {
	private String body = null;
	private int statusCode = 0;
	private Map<String, String> headers = null;
	private Integer fixedDelayMilliseconds = null;
	private DelayDistribution delayDistribution = null;
	private ChunkedDribbleDelay chunkedDribbleDelay = null;
	private Fault fault = null;
	private String proxyBaseUrl = null;
	private ResponseDefinitionBuilder responseMapping = null;
	
	public Response(Builder builder) {
		this.statusCode = builder.statusCode;
		this.headers = builder.headers;
		this.body = builder.body;
		this.fixedDelayMilliseconds = builder.fixedDelayMilliseconds;
		this.delayDistribution = builder.delayDistribution;
		this.chunkedDribbleDelay = builder.chunkedDribbleDelay;
		this.fault = builder.fault;
		this.proxyBaseUrl = builder.proxyBaseUrl;
		buildResponseMapping();
	}
	
	public static class Builder {
		private String body = null;
		private int statusCode = 0;
		private Map<String, String> headers = null;
		private Integer fixedDelayMilliseconds = null;
		private DelayDistribution delayDistribution = null;
		private ChunkedDribbleDelay chunkedDribbleDelay = null;
		private Fault fault = null;
		private String proxyBaseUrl = null;
		
		public Builder(int statusCode) {
			this.headers = new HashMap<String, String>();
			this.statusCode = statusCode;
		}
		
		public Builder addBody(String body) {
			this.body = body;
			return this;
		}
		
		public Builder addHeader(String key, String value) {
			this.headers.put(key, value);
			return this;
		}
		
		public Builder addFixedDelay(int milliseconds) {
			this.fixedDelayMilliseconds = milliseconds;
			return this;
		}
		
		public Builder addRandomDelay(DelayDistribution delayDistribution) {
			this.delayDistribution =  delayDistribution;
			return this;
		}
		
		public Builder addChunkedDribbleDelay(int numberOfChunks, int totalDuration) {
	        this.chunkedDribbleDelay = new ChunkedDribbleDelay(numberOfChunks, totalDuration);
	        return this;
	    }
		
		public Builder addFault(Fault fault) {
			this.fault = fault;
			return this;
		}
		
		public Builder addProxyBaseUrl(String proxyBaseUrl) {
			this.proxyBaseUrl = proxyBaseUrl;
			return this;
		}
		
		public Response build() {
			return new Response(this);
		}
	}
	
	private void buildResponseMapping() {
		this.responseMapping = aResponse();
		if (statusCode != 0) {
			this.responseMapping = this.responseMapping.withStatus(statusCode);
		}
		
		if (body != null) {
			this.responseMapping = this.responseMapping.withBody(this.body);
		}
		
		if (headers != null) {
			for (Map.Entry<String,String> headerEntry : this.headers.entrySet()) {
				this.responseMapping = this.responseMapping.withHeader(headerEntry.getKey(), headerEntry.getValue());
			}
		}
		if(fixedDelayMilliseconds != null) {
			this.responseMapping = this.responseMapping.withFixedDelay(fixedDelayMilliseconds);
		}
		if(delayDistribution != null) {
			this.responseMapping = this.responseMapping.withRandomDelay(delayDistribution);
		}
		if(this.chunkedDribbleDelay != null) {
			this.responseMapping = this.responseMapping.withChunkedDribbleDelay(chunkedDribbleDelay.getNumberOfChunks(), 
																				chunkedDribbleDelay.getTotalDuration());
		}
		if(this.fault != null) {
			this.responseMapping = this.responseMapping.withFault(fault);
		}
		if(this.proxyBaseUrl != null) {
			this.responseMapping = this.responseMapping.proxiedFrom(proxyBaseUrl);
		}
	}
	
	public String getBody() {
		return this.body;
	}
	
	public void setBody(String body) {
		this.body = body;
	}
	
	public int getStatusCode() {
		return this.statusCode;
	}
	
	public Map<String,String> getHeaders() {
		return this.headers;
	}
	
	public ResponseDefinitionBuilder getResponseMapping() {
		return this.responseMapping;
	}
	
	public Integer getFixedDelayMilliseconds() {
		return this.fixedDelayMilliseconds;
	}
	
	public DelayDistribution getDelayDistribution() {
		return this.delayDistribution;
	}
	
	public ChunkedDribbleDelay getChunkedDribbleDelay() {
		return this.chunkedDribbleDelay;
	}
	
	public Fault getFault() {
		return this.fault;
	}
	
	public String getProxyBaseUrl() {
		return this.proxyBaseUrl;
	}
}
