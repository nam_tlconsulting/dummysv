package com.tlconsulting.frameworks.service_virtualisation_framework;

import com.github.tomakehurst.wiremock.WireMockServer;
import com.github.tomakehurst.wiremock.core.WireMockConfiguration;
import com.github.tomakehurst.wiremock.matching.MatchResult;
import com.github.tomakehurst.wiremock.matching.RequestPattern;
import com.github.tomakehurst.wiremock.stubbing.StubMapping;
import com.google.common.reflect.TypeToken;
import com.tlconsulting.common.files.DirectoryWatch;
import com.tlconsulting.common.serializer.Serializer;

import java.io.IOException;
import java.lang.reflect.Type;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Properties;
import java.util.Set;
import java.util.UUID;

import org.apache.commons.lang3.StringUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import static com.github.tomakehurst.wiremock.core.WireMockConfiguration.options;
import static com.tlconsulting.common.files.FileOperations.*;

public class MockService {
	
	private static final String CONFIGURATION_DIRECTORY = "service virtualisation data";
	private static final String ENDPOINT_DIRECTORY = "endpoints";
	private static final String MAPPING_DIRECTORY = "mappings";
	private static final String ENDPOINT_MASTER_DIRECTORY = "master";
	private static final String ENDPOINT_MASTER_FILE = "endpoint-master.json";
	private static final Logger logger = LogManager.getLogger();
	private DirectoryWatch watchMappings = null;
	private DirectoryWatch watchEndPoints = null;
	private DirectoryWatch watchMasterEndPoints = null;
	private WireMockServer mockServer;
	private RequestInterceptor requestInterceptor;
	private PostResponse resetToPlayback;
	private ServiceParameters serviceParameters;
	private static Properties properties;
	private String absoluteUpstreamUrl;
	private List<EndPoint> endPoints;
	private String customHandlerHost = "";
	private int port;
	private boolean watcher = false;

	public MockService() {
		  this(ServiceParameters.Builder.newInstanceFromProperties(properties).build());
	}
	
	public MockService(Properties properties) {}
	
	public MockService(ServiceParameters params) {
		serviceParameters = params;
		requestInterceptor = new RequestInterceptor();
		resetToPlayback = new PostResponse();
		endPoints = new ArrayList<>();
		absoluteUpstreamUrl = serviceParameters.getScheme() + "://" + serviceParameters.getUpstream().getUpstreamUrl();
		
		WireMockConfiguration options = options().extensions(requestInterceptor, resetToPlayback).bindAddress(serviceParameters.getBindingAddress())
				.usingFilesUnderDirectory("./" + CONFIGURATION_DIRECTORY).jettyStopTimeout(10000L); // TODO configure the timeout value
		
		if (params.isHttps()) {
			if (params.getPort() == 0) {
				options = options.dynamicPort().dynamicHttpsPort();
			} else {
				options = options.dynamicPort().httpsPort(params.getPort());
			}
		} else {
			if (params.getPort() == 0) {
				options = options.dynamicPort();
			} else {
				options = options.port(params.getPort());
			}
		}

		createWireMockInstance(options);
		createDirectory(CONFIGURATION_DIRECTORY);
		createDirectory(CONFIGURATION_DIRECTORY + "/" + MAPPING_DIRECTORY);
		createDirectory(CONFIGURATION_DIRECTORY + "/" + ENDPOINT_DIRECTORY);
		createDirectory(CONFIGURATION_DIRECTORY + "/" + ENDPOINT_MASTER_DIRECTORY);
		createFile(CONFIGURATION_DIRECTORY + "/" + ENDPOINT_MASTER_DIRECTORY + "/" + ENDPOINT_MASTER_FILE);
		loadEndPoints();
		loadMasterFile();
	}

	public int getPort() {
		return port;
	}

	public ServiceParameters getServiceParameters() {
		return serviceParameters;
	}
	
	public void startService() {
		mockServer.start();
		port = this.serviceParameters.isHttps() ? mockServer.httpsPort() : mockServer.port();
		logger.info("Wiremock running on port: " + getPort());
		requestInterceptor.setMockService(this);
		resetToPlayback.setMockService(this);
		if(properties != null) 
			watcher = Boolean.parseBoolean(properties.getProperty("watcher.enabled", "false"));

		if (watcher)
			enableWatcher();
	}

	public void reloadScenarios() {
		logger.info("LOADING SCENARIOS from the filesystem.");
		mockServer.resetToDefaultMappings();
	}

	public void updateEndPoints() {
		logger.info("LOADING ENDPOINTS MASTER FILE from the filesystem.");
		watchEndPoints.suspend();
		loadMasterFile();
		watchEndPoints.resume();
	}

	public void reloadEndPoints() {
		logger.info("LOADING ENDPOINTS from the filesystem.");
		watchMasterEndPoints.suspend();
		loadEndPoints();
		saveEndPointsToMasterFile();
		watchMasterEndPoints.resume();
	}

	public void stopService() {
		mockServer.stop();
		if (watcher)
			disableWatcher();
	}

	public void enableWatcher() {
		logger.info("Directory watcher is ENABLED.");
		int sleep = Integer.parseUnsignedInt(properties.getProperty("watcher.sleep", "1000"));

		if (watchMappings != null || watchEndPoints != null)
			return;

		try {
			watchMappings = new DirectoryWatch("watchMappings", CONFIGURATION_DIRECTORY + "/" + MAPPING_DIRECTORY, this::reloadScenarios, sleep);
			watchMasterEndPoints = new DirectoryWatch("watchEndPointsMaster", CONFIGURATION_DIRECTORY + "/" + ENDPOINT_MASTER_DIRECTORY,
					this::updateEndPoints, sleep);
			watchEndPoints = new DirectoryWatch("watchEndPoints", CONFIGURATION_DIRECTORY + "/" + ENDPOINT_DIRECTORY, this::reloadEndPoints, sleep);
		} catch (IOException e) {
			logger.error(e.getMessage(), e);
		}
		watchMappings.startListener();
		watchEndPoints.startListener();
		watchMasterEndPoints.startListener();
	}

	public void disableWatcher() {
		watchMappings.shutDownListener();
		watchEndPoints.shutDownListener();
		watchMasterEndPoints.shutDownListener();
	}

	public WireMockServer getServer() {
		return mockServer;
	}

	public String getAbsoluteUpstreamUrl() {
		return absoluteUpstreamUrl;
	}
	
	public EndPoint addEndPoint(UUID id, String name, Mode mode, String url) {

		if (!isEndPointNameExisting(name)) {
			EndPoint newEndPoint = new EndPoint(id, name, mode, url, this);
			endPoints.add(newEndPoint);
			return newEndPoint;
		}
		return getEndPointByName(name);
	}

	public EndPoint getEndPointByUrl(String url) {
		Collections.sort(endPoints);
		for (EndPoint e : endPoints) {
			if (url.startsWith(e.getUrl())) {
				return e;
			}
		}
		
		UUID uuid = UUID.randomUUID();
		String urlWithoutParams = removeUrlParams(url);
		return addEndPoint(uuid, generateEndPointName(urlWithoutParams), getDefaultMode(), urlWithoutParams);
	}
	
	public List<String> getEndPointUrls(List<EndPoint> endpoints) {
		List<String> urls = new ArrayList<>();
		for (EndPoint endPoint : endpoints) {
			if (!endPoint.getMode().equals(Mode.UNDEFINED)) {
				urls.add(endPoint.getUrl());
			}
		}
		return urls;
	}

	public void removeEndPointByName(String endPointName) {
		List<UUID> scenarioIds = new ArrayList<>();
		Iterator<EndPoint> endPointIterator = endPoints.iterator();

		while (endPointIterator.hasNext()) {
			EndPoint endPoint = endPointIterator.next();
			if (endPoint.getName().equals(endPointName)) {
				scenarioIds = endPoint.getScenarioIds();
				endPointIterator.remove();
				deleteFile(CONFIGURATION_DIRECTORY + "/" + ENDPOINT_DIRECTORY + "/" + endPointName + ".json");
			}
		}

		for (UUID scenarioId : scenarioIds) {
			getServer().removeStub(getServer().getSingleStubMapping(scenarioId));
		}
	}

	public boolean isRequestMatchedWithAnyScenarios(EndPoint endPoint,
			com.github.tomakehurst.wiremock.http.Request request) {
		List<StubMapping> mappings = endPoint.getStubMappings(this, endPoint);

		if (mappings == null || mappings.isEmpty()) {
			return false;
		}

		for (StubMapping m : mappings) {
			if (m == null)
				continue;
			RequestPattern requestPattern = m.getRequest();
			if (requestPattern != null) {
				MatchResult matchResult = requestPattern.match(request);

				double distance = matchResult.getDistance();
				if (distance == 0) {
					return true;
				}
			}
		}
		return false;
	}

	public static void setProperties(Properties prop) {
		properties = prop;
	}

	public static Properties getProperties() {
		return properties;
	}
	
	public String getCustomHandler() {
		return customHandlerHost + serviceParameters.getcustomHandlerUrl();
	}
	
	public void setCustomHandlerHost(String value) {
		customHandlerHost = value;
	}

	public void updateStub(StubMapping stubMapping) {
		this.getServer().editStubMapping(stubMapping);
	}

	public String getEndPointsFolder() {
		return CONFIGURATION_DIRECTORY + "/" + ENDPOINT_DIRECTORY;
	}
	
	public void setGlobalFixedDelay(int fixedDelayMiliseconds) {
		//TODO
		// Set or remove global fixed delay
		// mockServer.setGlobalFixedDelay(0);
	}
	
	public String getMappingsFolder() {
		return CONFIGURATION_DIRECTORY + "/" + MAPPING_DIRECTORY;
	}

	public String removeUrlParams(String url) {
		URI uri = null;
		try {
			uri = new URI(url);
		} catch (URISyntaxException e) {
			logger.error("Error when remove params from url: {}", e);
		}
		return uri.getPath();
	}
	
	private Mode getDefaultMode() {		
		return properties != null && StringUtils.isNotEmpty(properties.getProperty("default.mode")) 
							? Mode.valueOf(properties.getProperty("default.mode").toUpperCase()) 
							: Mode.UNDEFINED;
	}
	
	private String generateEndPointName(String url) {
		url = url.replaceFirst("/", ".");
		url = url.replaceAll("/", "_");
		return serviceParameters.getUpstream().getName() + url;
	}
	
	private void createWireMockInstance(WireMockConfiguration options) {
		mockServer = new WireMockServer(options);
	}
	
	private void loadEndPoints() {
		endPoints = initialiseObjectsFromFiles(CONFIGURATION_DIRECTORY + "/" + ENDPOINT_DIRECTORY, EndPoint.class);

		if (endPoints == null)
			return;

		for (EndPoint endPoint : endPoints) {
			endPoint.setParentMockService(this);
		}

		if (hasDuplicate(getEndPointUrls(endPoints))) {
			throw new IllegalArgumentException("Similar urls in two or more end points " + getEndPointUrls(endPoints));
		}
	}

	private EndPoint getEndPointByName(String name) {
		for (EndPoint e : endPoints) {
			if (name.equals(e.getName())) {
				return e;
			}
		}
		UUID uuid = UUID.randomUUID();
		return addEndPoint(uuid, name + uuid, Mode.UNDEFINED, "Undefined" + uuid);
	}

	private List<String> getEndPointNames(List<EndPoint> endpoints) {
		List<String> names = new ArrayList<>();
		for (EndPoint endPoint : endpoints) {
			names.add(endPoint.getName());
		}
		return names;
	}

	private boolean isEndPointNameExisting(String name) {
		List<String> endPointNames = getEndPointNames(endPoints);
		for (String n : endPointNames) {
			if (n.equals(name)) {
				return true;
			}
		}
		return false;
	}

	private <T> boolean hasDuplicate(Collection<T> list) {
		if (getDuplicate(list).isEmpty())
			return false;
		return true;
	}

	private <T> List<T> getDuplicate(Collection<T> list) {
		List<T> duplicatedObjects = new ArrayList<T>();
		Set<T> set = new HashSet<T>() {
			private static final long serialVersionUID = 1L;

			@Override
			public boolean add(T e) {
				if (contains(e)) {
					duplicatedObjects.add(e);
				}
				return super.add(e);
			}
		};
		for (T t : list) {
			set.add(t);
		}
		return duplicatedObjects;
	}

	private <T> List<T> initialiseObjectsFromFiles(String dirName, Class<T> classType) {
		List<T> objects = new ArrayList<>();
		List<String> jsonStrings = readTextFiles(dirName);
		for (String jsonString : jsonStrings) {
			T object = Serializer.jsonToObject(jsonString, classType);
			objects.add(object);
		}
		return objects;
	}

	/**
	 * endpoints master file implementation
	 */
	private void saveEndPointsToMasterFile() {
		@SuppressWarnings("serial")
		Type listType = new TypeToken<ArrayList<EndPoint>>() {
		}.getType();
		String content = Serializer.toJSON(endPoints, listType, new EndPointSummaryStrategy());
		writeTextFile(content, CONFIGURATION_DIRECTORY + "/" + ENDPOINT_MASTER_DIRECTORY + "/" + ENDPOINT_MASTER_FILE);
	}

	private void loadMasterFile() {
		logger.info("Processing Master endpoints file");
		String jsonString = readTextFile(CONFIGURATION_DIRECTORY + "/" + ENDPOINT_MASTER_DIRECTORY + "/" + ENDPOINT_MASTER_FILE);
		if (jsonString.equals("")) {
			saveEndPointsToMasterFile();
			jsonString = readTextFile(CONFIGURATION_DIRECTORY + "/" + ENDPOINT_MASTER_DIRECTORY + "/" + ENDPOINT_MASTER_FILE);
		}
		EndPoint[] loadedEndPoints = Serializer.jsonToObject(jsonString, EndPoint[].class);
		if (loadedEndPoints == null)
			return;
		try {
			for (EndPoint newEndPoint : loadedEndPoints) {
				String endPointName = newEndPoint.getName();
				EndPoint match = endPoints.stream().filter(endPoint -> endPointName.equals(endPoint.getName()))
						.findAny().orElse(null);
				if (match != null) {
					if (!match.getMode().equals(newEndPoint.getMode()))
						match.setMode(newEndPoint.getMode());
				}
			}
		} catch (Exception e) {
			logger.error("master file is corrupt. Please delete so it can be regenerated.");
			throw e;
		}

	}
}