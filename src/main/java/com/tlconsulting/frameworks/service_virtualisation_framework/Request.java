package com.tlconsulting.frameworks.service_virtualisation_framework;

import static com.github.tomakehurst.wiremock.client.WireMock.any;
import static com.github.tomakehurst.wiremock.client.WireMock.containing;
import static com.github.tomakehurst.wiremock.client.WireMock.delete;
import static com.github.tomakehurst.wiremock.client.WireMock.equalTo;
import static com.github.tomakehurst.wiremock.client.WireMock.equalToJson;
import static com.github.tomakehurst.wiremock.client.WireMock.equalToXml;
import static com.github.tomakehurst.wiremock.client.WireMock.get;
import static com.github.tomakehurst.wiremock.client.WireMock.matching;
import static com.github.tomakehurst.wiremock.client.WireMock.matchingJsonPath;
import static com.github.tomakehurst.wiremock.client.WireMock.matchingXPath;
import static com.github.tomakehurst.wiremock.client.WireMock.notMatching;
import static com.github.tomakehurst.wiremock.client.WireMock.options;
import static com.github.tomakehurst.wiremock.client.WireMock.patch;
import static com.github.tomakehurst.wiremock.client.WireMock.post;
import static com.github.tomakehurst.wiremock.client.WireMock.put;
import static com.github.tomakehurst.wiremock.client.WireMock.trace;
import static com.github.tomakehurst.wiremock.client.WireMock.urlPathMatching;

import java.util.ArrayList;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.github.tomakehurst.wiremock.client.MappingBuilder;
import com.github.tomakehurst.wiremock.http.RequestMethod;
import com.github.tomakehurst.wiremock.matching.StringValuePattern;
import com.tlconsulting.common.httpCompare.BasicAuthCredential;
import com.tlconsulting.common.httpCompare.BodyPattern;
import com.tlconsulting.common.httpCompare.Cookie;
import com.tlconsulting.common.httpCompare.Header;
import com.tlconsulting.common.httpCompare.QueryParameter;
import com.tlconsulting.common.comparer.MatchOperation;

public class Request {
	
	private String urlPath = null;
	private RequestMethod method = null;
	private String body = null;
	private ArrayList<Header> headers = null;
	private ArrayList<Cookie> cookies = null;
	private ArrayList<BodyPattern> bodyPatterns = null;
	private ArrayList<QueryParameter> queryParameters = null;
	private BasicAuthCredential basicAuthCredential = null;
	
	private static final Logger logger = LogManager.getLogger();
	
	private MappingBuilder RequestMapping = null;
	
	public Request (Builder builder) {
		this.urlPath = builder.urlPath;
		this.method = builder.method;
		this.headers = builder.headers;
		this.cookies = builder.cookies;
		this.bodyPatterns = builder.bodyPatterns;
		this.queryParameters = builder.queryParameters;
		this.basicAuthCredential = builder.basicAuthCredential;
		this.body = builder.body;
		buildRequestMapping();
	}
	
	public String getBody() {
		return body;
	}
	
	public String getUrl() {
		return urlPath;
	}
	
	public ArrayList<QueryParameter> getQueryParams() {
		return queryParameters;
	}
	
	public String getQueryString() {
		ArrayList<String> queryStrings = new ArrayList<String>();
		for (QueryParameter entry : queryParameters) {
			queryStrings.add(entry.key + "=" + entry.value);
		}
		return String.join("&", queryStrings);
	}
	
	public static class Builder {
		private String urlPath = null;
		private RequestMethod method = null;
		private String body = "";
		private ArrayList<Header> headers = null;
		private ArrayList<Cookie> cookies = null;
		private ArrayList<BodyPattern> bodyPatterns = null;
		private ArrayList<QueryParameter> queryParameters = null;
		private BasicAuthCredential basicAuthCredential = null;
		
		public Builder(String urlPath, RequestMethod method) {
			this.urlPath = urlPath;
			this.method = method;
			this.headers = new ArrayList<Header>();
			this.cookies = new ArrayList<Cookie>();
			this.bodyPatterns = new ArrayList<BodyPattern>();
			this.queryParameters = new ArrayList<QueryParameter>();
		}
		
		public Builder addHeader(String key, MatchOperation operation, String value) {
			headers.add(new Header(key, operation, value));
			return this;
		}
		
		public Builder removeHeader(String key) {
			for(Header header : headers) {
				if (key.equals(header.key)) {
					headers.remove(header);
				}
			}
			return this;
		}
		
		public Builder addCookie(String key, MatchOperation operation, String value) {
			cookies.add(new Cookie(key, operation, value));
			return this;
		}
		
		public Builder removeCookie(String key) {
			for(Cookie cookie : cookies) {
				if (key.equals(cookie.key)) {
					cookies.remove(cookie);
				}
			}
			return this;
		}
		
		public Builder addQueryParameter(String key, MatchOperation operation, String value) {
			queryParameters.add(new QueryParameter(key, operation, value));
			return this;
		}
		
		public Builder removeQueryParameter(String key) {
			for(QueryParameter queryParameter : queryParameters) {
				if (key.equals(queryParameter.key)) {
					queryParameters.remove(queryParameter);
				}
			}
			return this;
		}
		
		public Builder addBody(String value) {
			this.body = value;
			return this;
		}
		
		public Builder addBodyPattern(MatchOperation operation, String value) {
			bodyPatterns.add(new BodyPattern(operation, value));
			return this;
		}
		
		public Builder removeBodyPattern(String value) {
			for(BodyPattern bodyPattern : bodyPatterns) {
				if (value.equals(bodyPattern.value)) {
					bodyPatterns.remove(bodyPattern);
				}
			}
			return this;
		}
		
		public Builder addBasicAuthentication(String username, String password) {
			this.basicAuthCredential = new BasicAuthCredential(username, password);
			return this;
		}
		
		public Request build() {
			return new Request(this);
		}
	}
	
	private Request buildRequestMapping() {
		
		// method and url
		if (this.method != null && this.urlPath != null) {
			this.RequestMapping = buildMethodAndURL(this.method,this.urlPath);
		} else {
			logger.fatal("Could not initialize Request. Field: Method and urlPath must not be null");
		}
		
		// bodyPatterns
		if (this.bodyPatterns != null) {
			for (BodyPattern bodyPatternsEntry : this.bodyPatterns) {
				
				// add bodyPattern to RequestMapping
				this.RequestMapping = this.RequestMapping.withRequestBody(MatchOperation(bodyPatternsEntry.operation, bodyPatternsEntry.value));
			} 
		}
		
		// cookies
		if (this.cookies != null) {
			for (Cookie cookieEntry : this.cookies) {
				
				// add cookies to RequestMapping
				this.RequestMapping = this.RequestMapping.withCookie(cookieEntry.key, MatchOperation(cookieEntry.operation, cookieEntry.value));
			}
		}
		
		// headers
		if (this.headers != null) {
			for (Header headerEntry : this.headers) {
				
				// add headers to RequestMapping
				this.RequestMapping = this.RequestMapping.withHeader(headerEntry.key, MatchOperation(headerEntry.operation, headerEntry.value));
			}
		}
		
		// queryParameters
		if (this.queryParameters != null) {
			for (QueryParameter searchTermEntry : this.queryParameters) {
				
				// add queryParameters to RequestMapping
				this.RequestMapping = this.RequestMapping.withQueryParam(searchTermEntry.key, MatchOperation(searchTermEntry.operation, searchTermEntry.value));
			}
		}
		
		// basicAuthentication
		if (this.basicAuthCredential != null) {
			this.RequestMapping = this.RequestMapping.withBasicAuth(this.basicAuthCredential.username, this.basicAuthCredential.password);
		}
		return this;
	}
	
	// Accessories Method
	private static StringValuePattern MatchOperation(MatchOperation operation, String value) {
		StringValuePattern svp = null;
		switch(operation) {
			case contains:
				svp = containing(value);
				break;
			case doesNotMatch:
				svp = notMatching(value);
				break;
			case equalTo:
				svp = equalTo(value);
				break;
			case equalToJson:
				svp = equalToJson(value);
				break;
			case equalToXml:
				svp = equalToXml(value);
				break;
			case matches:
				svp = matching(value);
				break;
			case matchesJsonPath:
				svp = matchingJsonPath(value);
				break;
			case matchesXPath:
				svp = matchingXPath(value);
				break;
			default:
				svp = containing(value);
				break;
					
		}
		return svp;
	}
	
	private MappingBuilder buildMethodAndURL(RequestMethod method, String urlPath) {
		switch(method.getName()) {
			case "GET":
				return get(urlPathMatching(urlPath));
			case "POST":
				return post(urlPathMatching(urlPath));
			case "PUT":
				return put(urlPathMatching(urlPath));
			case "PATCH":
				return patch(urlPathMatching(urlPath));
			case "DELETE":
				return delete(urlPathMatching(urlPath));
			case "OPTIONS":
				return options(urlPathMatching(urlPath));
			case "TRACE":
				return trace(urlPathMatching(urlPath));
			case "ANY":
				return any(urlPathMatching(urlPath));
			default:
				return any(urlPathMatching(urlPath));
		}
	}
	// Accessories Method END
	
	// Getters & Setters
	public MappingBuilder getRequestMapping() {
		return this.RequestMapping;
	}
	
	public RequestMethod getRequestMethod() {
		return this.method;
	}
	// Getters & Setters END
}