package com.tlconsulting.frameworks.service_virtualisation_framework;
import static com.github.tomakehurst.wiremock.core.Options.DEFAULT_BIND_ADDRESS;

import java.util.Properties;

import com.github.tomakehurst.wiremock.http.ChunkedDribbleDelay;
import com.github.tomakehurst.wiremock.http.DelayDistribution;
import com.github.tomakehurst.wiremock.http.LogNormal;
import com.github.tomakehurst.wiremock.http.UniformDistribution;

public class ServiceParameters {

	private String host;
	private String bindingAddress;
	private String customHandlerUrl = "";
	private Upstream upstream;
	private int port;
	private boolean isHttps;
	private boolean captureResponseTimes;
	private ChunkedDribbleDelay chunkDribbleDelay;
	private DelayDistribution delayDistribution;
    private static final int DEFAULT_HTTP_PORT = 80;
	private static final int DEFAULT_HTTPS_PORT = 443;
	
	public ServiceParameters(Builder builder) {
		this.host = builder.host;
		this.bindingAddress = builder.bindingAddress;
		this.customHandlerUrl = builder.customHandlerUrl;
		this.upstream = builder.upstream;
		this.port = builder.port;
		this.isHttps = builder.isHttps;
		this.captureResponseTimes = builder.captureResponseTimes;
		this.chunkDribbleDelay = builder.chunkDribbleDelay;
		this.delayDistribution = builder.delayDistribution;
	}
	
	public static class Builder {
		private String host = "127.0.0.1";
		private String bindingAddress = DEFAULT_BIND_ADDRESS;
		private String customHandlerUrl = "";
		private Upstream upstream;
		private int port = 0;
		private boolean isHttps;
		private boolean captureResponseTimes;
		private ChunkedDribbleDelay chunkDribbleDelay = null;
		private DelayDistribution delayDistribution = null;
		
		public static Builder newInstance(String upstreamHost, String upstreamName) {
			return new Builder(upstreamHost, upstreamName);
		}
		
		public static Builder newInstanceFromProperties(Properties properties) {
			return new Builder(properties);
		}
		
		private Builder(String upstreamHost, String upstreamName) {
			this.upstream = new Upstream(upstreamHost, upstreamName);
		}
		
		private Builder(Properties properties) {
			this.upstream = new Upstream(properties.getProperty("upstream.host"), 
										Integer.parseInt(properties.getProperty("upstream.port")), 
										properties.getProperty("upstream.name"));   
			this.setHttps(Boolean.parseBoolean(properties.getProperty("https.enabled")));
			this.captureResponseTimes = Boolean.parseBoolean(properties.getProperty("capture.response.times"));
					
			if(properties.getProperty("chunk.number").length() > 0 && properties.getProperty("total.duration").length() > 0) {
				int numberOfChunks = Integer.parseInt(properties.getProperty("chunk.number"));
				int totalDuration = Integer.parseInt(properties.getProperty("total.duration"));
				this.chunkDribbleDelay = new ChunkedDribbleDelay(numberOfChunks, totalDuration);
			}
		
			if(properties.getProperty("lognormal.median").length() > 0 && properties.getProperty("lognormal.sigma").length() > 0) {
				double median = Double.parseDouble(properties.getProperty("lognormal.median"));
				double sigma = Double.parseDouble(properties.getProperty("lognormal.sigma"));
				this.delayDistribution = new LogNormal(median, sigma);
			}
			
			if(properties.getProperty("uniform.distribution.lower").length() > 0 && properties.getProperty("uniform.distribution.upper").length() > 0) {
				int lower = Integer.parseInt(properties.getProperty("uniform.distribution.lower"));
				int upper = Integer.parseInt(properties.getProperty("uniform.distribution.upper"));
				this.delayDistribution = new UniformDistribution(lower, upper);
			}
			
			if(properties.getProperty("custom.handler").length() > 0 ) {
				this.customHandlerUrl = properties.getProperty("custom.handler");
			}	
		}
		
		public Builder setHost(String host) {
			this.host = host;
			return this;
		}
		
		public Builder setBindingAddress(String bindingAddress) {
			this.bindingAddress = bindingAddress;
			return this;
		}
		
		public Builder setCustomHandlerUrl(String customHandlerUrl) {
			this.customHandlerUrl = customHandlerUrl;
			return this;
		}
		
		public Builder setUpstream(Upstream upstream) {
			this.upstream = upstream;
			return this;
		}
		
		public Builder setUpstreamPort(int port) {
			this.upstream.setUpstreamPort(port);
			return this;
		}
		
		public Builder setPort(int port) {
			this.port = port;
			return this;
		}
		
		public Builder setHttps(boolean isHttps) {
			this.isHttps = isHttps;
			return this;
		}
		
		public ServiceParameters build() {
			return new ServiceParameters(this);
		}
	}
	
	public int getPort() {
		return port;
	}
	
	public String getcustomHandlerUrl() {
		return customHandlerUrl;
	}
	
	public void setcustomHandlerUrl(String value) {
		this.customHandlerUrl = value;
	}

	public String getHost() {
		return host;
	}

	public String getBindingAddress() {
		return bindingAddress;
	}
	
	public boolean isHttps() {
		return isHttps;
	}
	
	public boolean isCaptureResponseTimes() {
		return captureResponseTimes;
	}
	
	public ChunkedDribbleDelay getChunkDribbleDelay() {
		return chunkDribbleDelay;
	}

	public DelayDistribution getDelayDistribution() {
		return delayDistribution;
	}

	public String getScheme() {
		return isHttps() ? "https" : "http"; 
	}
	
	public Upstream getUpstream() {
		return upstream;
	}
}
