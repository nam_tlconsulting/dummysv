package com.tlconsulting.frameworks.service_virtualisation_framework;

import static com.tlconsulting.common.comparer.MatchOperation.contains;
import static com.tlconsulting.common.comparer.MatchOperation.equalTo;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Enumeration;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.github.tomakehurst.wiremock.http.RequestMethod;
import com.tlconsulting.common.httpCompare.QueryParameter;
import com.tlconsulting.frameworks.service_virtualisation_framework.Request.Builder;

public abstract class CustomHandler extends HttpServlet {

	/**
	 * 
	 */
	private static final long serialVersionUID = 3915082117722766038L;
	private static final Logger logger = LogManager.getLogger();

	public abstract Response getResponse(Request request);

	public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		handleRequest(request, response);
	}

	public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		handleRequest(request, response);
	}

	public void doPut(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		handleRequest(request, response);
	}

	public void doDelete(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		handleRequest(request, response);
	}

	private void handleRequest(HttpServletRequest request, HttpServletResponse response) {
		try {
			String endpoint = request.getRequestURI().substring(request.getContextPath().length() + request.getServletPath().length());
			logger.debug("PASSTHROUGH to CustomHandler: [" + request.getMethod() + " " + endpoint + "]");

			Builder customRequestBuilder = new Request.Builder(endpoint, new RequestMethod(request.getMethod()));
			customRequestBuilder = copyHeadersToRequest(request, customRequestBuilder);

			if (!request.getMethod().equals("GET")) {
				String str, body = "";
				BufferedReader input = request.getReader();
				while ((str = input.readLine()) != null) {
					body += str;
				}
				customRequestBuilder = customRequestBuilder.addBody(body);
			} else {
				
				customRequestBuilder = copyQueryStringsToRequest(request, customRequestBuilder);
			}

			Request customRequest = customRequestBuilder.build();

			Response customResponse = getResponse(customRequest);

			response.setStatus(customResponse.getStatusCode());
			copyHeadersToResponse(customResponse, response);

			PrintWriter output = response.getWriter();
			output.println(customResponse.getBody());

		} catch (IOException e) {
			logger.error(e.getMessage(), e);
		}
	}

	private Builder copyQueryStringsToRequest(HttpServletRequest request, Builder builder) {
		HttpServletRequest httpRequest = (HttpServletRequest) request;
		Map<String, List<String>> queryParams = QueryParameter.splitQuery(httpRequest.getQueryString());

		for (Map.Entry<String, List<String>> entry : queryParams.entrySet()) {
			String queryName = entry.getKey();
			String queryValue = String.join(",", entry.getValue());
			builder.addQueryParameter(queryName, equalTo, queryValue);
		}
		return builder;
	}

	private Builder copyHeadersToRequest(HttpServletRequest request, Builder builder) {
		HttpServletRequest httpRequest = (HttpServletRequest) request;
		Enumeration<String> headerNames = httpRequest.getHeaderNames();
		if (headerNames != null) {
			logger.debug("copy request headers");
			while (headerNames.hasMoreElements()) {
				String nextHeaderName = headerNames.nextElement();
				String headerValue = httpRequest.getHeader(nextHeaderName);
				if (nextHeaderName.toLowerCase().contentEquals("accept-encoding"))
					continue;
				builder.addHeader(nextHeaderName, contains, headerValue);
				logger.debug(nextHeaderName + ": " + headerValue);
			}
		}
		return builder;
	}

	private void copyHeadersToResponse(Response customResponse, HttpServletResponse response) {
		logger.debug("copy response headers");
		for (Map.Entry<String, String> entry : customResponse.getHeaders().entrySet()) {
			String headerName = entry.getKey();
			String headerValue = entry.getValue();
			response.setHeader(headerName, headerValue);
			logger.debug(headerName + ": " + headerValue);
		}
	}
}
