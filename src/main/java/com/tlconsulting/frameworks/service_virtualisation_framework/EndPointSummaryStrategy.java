package com.tlconsulting.frameworks.service_virtualisation_framework;

import com.google.gson.ExclusionStrategy;
import com.google.gson.FieldAttributes;

public class EndPointSummaryStrategy implements ExclusionStrategy {
	
	@Override
	public boolean shouldSkipField(FieldAttributes f) {
        return !((f.getDeclaringClass() == EndPoint.class && f.getName().equals("name"))||
        (f.getDeclaringClass() == EndPoint.class && f.getName().equals("mode")));
    }

	@Override
	public boolean shouldSkipClass(Class<?> clazz) {
		// TODO Auto-generated method stub
		return false;
	}

}
