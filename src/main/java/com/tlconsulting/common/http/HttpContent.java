package com.tlconsulting.common.http;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import org.apache.http.Header;
import org.apache.http.HttpEntity;

public class HttpContent implements HttpEntity{

	String content;
	
	@Override
	public boolean isRepeatable() {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean isChunked() {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public long getContentLength() {
		// TODO Auto-generated method stub
		return content.length();
	}

	@Override
	public Header getContentType() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Header getContentEncoding() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public InputStream getContent() throws IOException, UnsupportedOperationException {
		InputStream stream = new ByteArrayInputStream(content.getBytes());
		return stream;
	}

	@Override
	public void writeTo(OutputStream outstream) throws IOException {
		outstream.write(content.getBytes());	
	}

	@Override
	public boolean isStreaming() {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public void consumeContent() throws IOException {
		// TODO Auto-generated method stub
		
	}

}
