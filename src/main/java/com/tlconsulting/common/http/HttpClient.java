package com.tlconsulting.common.http;

import java.io.IOException;
import java.net.URISyntaxException;
import java.net.URL;

import org.apache.http.HttpResponse;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;

public class HttpClient {

	private String scheme;
	private String host;
	private int port;
	private final String USER_AGENT = "Mozilla/5.0";

	public HttpClient(String host) {
		this("http", host, 80);
	}

	public HttpClient(String host, int port) {
		this("http", host, port);
	}
	
	public HttpClient(String scheme, String host, int port) {
		this.scheme = scheme;
		this.host = host;
		this.port = port;
	}

	public String getHost() {
		return host;
	}

	public void setHost(String host) {
		this.host = host;
	}

	public int getPort() {
		return port;
	}

	public void setPort(int port) {
		this.port = port;
	}

	private CloseableHttpClient buildClient() {
		int timeout = 60;
		RequestConfig config = RequestConfig.custom().setSocketTimeout(timeout * 1000)
				.setConnectionRequestTimeout(timeout * 1000).setConnectTimeout(timeout * 1000).build();

		return HttpClients.custom().setDefaultRequestConfig(config).build(); // builder.build();

	}

	public HttpResponse get(String endpoint) throws IOException, URISyntaxException {
		URL url = new URL(scheme + "://" + host + ":" + port + endpoint);
		HttpGet httpGet = new HttpGet(url.toString());
		httpGet.addHeader("User-Agent", USER_AGENT);
		CloseableHttpClient httpClient = buildClient();
		HttpResponse response = httpClient.execute(httpGet);
		return response;
	}
}
