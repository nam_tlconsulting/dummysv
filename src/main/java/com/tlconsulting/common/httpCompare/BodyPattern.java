package com.tlconsulting.common.httpCompare;

import com.tlconsulting.common.comparer.MatchOperation;
import com.tlconsulting.common.comparer.ValueOperation;

public class BodyPattern extends ValueOperation{

	public BodyPattern(MatchOperation operation, String value) {
		super(operation, value);
	}
	
}
