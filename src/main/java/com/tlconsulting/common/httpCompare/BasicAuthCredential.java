package com.tlconsulting.common.httpCompare;

public class BasicAuthCredential {
	public String username;
	public String password;
	
	public BasicAuthCredential(String username, String password) {
		this.username = username;
		this.password = password;
	}
}
