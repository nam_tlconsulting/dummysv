package com.tlconsulting.common.httpCompare;

import java.util.Arrays;
import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.AbstractMap.SimpleImmutableEntry;
import java.util.stream.Collectors;

import com.google.common.base.Strings;
import com.tlconsulting.common.comparer.KeyValueOperation;
import com.tlconsulting.common.comparer.MatchOperation;

public class QueryParameter extends KeyValueOperation {

	public QueryParameter(String key, MatchOperation operation, String value) {
		super(key, operation, value);			
	}
	
	public static Map<String, List<String>> splitQuery(String queryString) {
		if (Strings.isNullOrEmpty(queryString)) {
			return Collections.emptyMap();
		}
		return Arrays.stream(queryString.split("&")).map(s -> splitQueryParameter(s)).collect(Collectors
				.groupingBy(SimpleImmutableEntry::getKey, LinkedHashMap::new, Collectors.mapping(Map.Entry::getValue, Collectors.toList())));
	}
	
	private static SimpleImmutableEntry<String, String> splitQueryParameter(String it) {
		final int idx = it.indexOf("=");
		final String key = idx > 0 ? it.substring(0, idx) : it;
		final String value = idx > 0 && it.length() > idx + 1 ? it.substring(idx + 1) : null;
		return new SimpleImmutableEntry<>(key, value);
	}
}