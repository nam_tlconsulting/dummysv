package com.tlconsulting.common.httpCompare;

import com.tlconsulting.common.comparer.KeyValueOperation;
import com.tlconsulting.common.comparer.MatchOperation;

public class Header extends KeyValueOperation {

	public Header(String key, MatchOperation operation, String value) {
		super(key, operation, value);			
	}
}