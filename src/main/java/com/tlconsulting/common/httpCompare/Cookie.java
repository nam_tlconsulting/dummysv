package com.tlconsulting.common.httpCompare;

import com.tlconsulting.common.comparer.KeyValueOperation;
import com.tlconsulting.common.comparer.MatchOperation;

public class Cookie extends KeyValueOperation {

	public Cookie(String key, MatchOperation operation, String value) {
		super(key, operation, value);			
	}
}