package com.tlconsulting.common.files;

import java.io.IOException;
import java.nio.file.FileSystems;
import java.nio.file.Path;
import java.nio.file.WatchEvent;
import java.nio.file.WatchKey;
import java.nio.file.WatchService;
import java.util.Map;
import java.util.UUID;
import java.util.concurrent.ConcurrentHashMap;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import static java.nio.file.StandardWatchEventKinds.*;

public class DirectoryWatch {

	private static final Logger logger = LogManager.getLogger();
	private final Map<WatchKey, Path> keyPaths = new ConcurrentHashMap<WatchKey, Path>();
	private final Runnable eventProcessor;
	private final WatchService watcher;
	private volatile Thread processingThread;
	private int sleep = 500;
	private final String name;
	private String threadName;
	private volatile boolean suspended;

	public DirectoryWatch(String name, String directory, Runnable eventProcessor, int sleep) throws IOException {
		this.name = name;
		this.eventProcessor = eventProcessor;
		this.sleep = sleep;
		watcher = FileSystems.getDefault().newWatchService();
		Path path = FileSystems.getDefault().getPath(directory);
		WatchKey key = path.register(watcher, ENTRY_CREATE, ENTRY_DELETE, ENTRY_MODIFY);
		keyPaths.put(key, path);
	}

	public void startListener() {
		while(processingThread != null) {
			logger.debug("Waiting for previous thread: "+ threadName + " to stop first before starting a new thread");
		}
		threadName = name + "-" + UUID.randomUUID();
		logger.debug("Startng thread: " + threadName );
		processingThread = new Thread(threadName) {
			public void run() {
				try {
					processFileNotifications();
				} catch (InterruptedException e) {
					logger.error(e.getMessage(), e);
					processingThread = null;
				}
				finally {
					logger.debug("Thread stopped: " + threadName );
					processingThread = null;
				}
			}
		};
		processingThread.start();
		logger.debug("Thread started: " + threadName );
	}
	
	public void suspend() {
		logger.debug("Stop listening for changes: " + threadName );
		suspended = true;
	}
	
	public void resume() {
		logger.debug("Resume listening for changes: " + threadName );
		suspended = false;
	}

	public void shutDownListener() {
		logger.debug("Stopping thread: " + threadName );
		if (processingThread != null) {
			processingThread.interrupt();
		}
	}

	@SuppressWarnings("rawtypes")
	private void processFileNotifications() throws InterruptedException {
		while (!processingThread.isInterrupted()) {
			while (!suspended)
			{
				WatchKey key = watcher.take();
				Path dir = keyPaths.get(key);
				for (WatchEvent evt : key.pollEvents()) {
					WatchEvent.Kind eventType = evt.kind();
					if (eventType == OVERFLOW)
						continue;
					Object o = evt.context();
					if (o instanceof Path) {
						Path path = (Path) o;
						process(dir, path, eventType);
					}
				}
				key.reset();
			}
		}
		try {
			watcher.close();
		} catch (IOException e) {
			logger.error(e.getMessage(),e);
		}
	}

	@SuppressWarnings("rawtypes")
	private void process(Path dir, Path file, WatchEvent.Kind evtType) throws InterruptedException {
		logger.info("A change has been detected in the file <" + file.getFileName() + ">");
		//TODO configure sleep time
		suspended = true;
		Thread.sleep(sleep);
		eventProcessor.run();
		suspended = false;
	}
}
