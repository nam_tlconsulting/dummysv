package com.tlconsulting.common.files;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.NoSuchFileException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class FileOperations {
	private static final Logger logger = LogManager.getLogger();
	
	public static String readTextFile(String filename)
	{
		String output = "";
		String line = null;

		try {
			FileReader fileReader = 
				new FileReader(filename);

			BufferedReader bufferedReader = 
				new BufferedReader(fileReader);

			while((line = bufferedReader.readLine()) != null) {
			output += line;
			}   

			bufferedReader.close();
		} catch(FileNotFoundException ex) {
			logger.error("Unable to open file: " + filename, ex);                
		} catch(IOException ex) {
			logger.error("Error reading file: " + filename, ex);                  
		}
		return output;
	}

	public static void writeTextFile(String text, String filename) {
		Path path = Paths.get(filename);
		logger.info("Writing to file: " + path.toAbsolutePath());
		try (BufferedWriter writer = Files.newBufferedWriter(path, Charset.forName("UTF-8"))) {
			writer.write(text);
		} catch (IOException ex) {
			logger.error("Error writing to file: " + filename, ex);
		}
	}
	
	public static void deleteFile(String filename) {
		logger.info("Deleting file: " + filename);
		try {
			Files.deleteIfExists(Paths.get(filename));
		} catch (NoSuchFileException e) {
			logger.error("No such " + filename + " exists");
		} catch (IOException e) {
			logger.error("Error deleting file: "+ filename);
		}
	}
	
	public static void createDirectory(String dirName) {
		File dir = new File(dirName);
		dir.mkdir();
	}
	
	public static List<String> readTextFiles(String dirName) {
		File dir = new File(dirName);
		File[] files = dir.listFiles();
		List<String> objects = new ArrayList<>();
		
		for(File file: files) {
			if(file.isFile() && file.getName().contains(".json")) {
				String jsonString = readTextFile(dirName + "/" + file.getName());
				objects.add(jsonString);
			}
		}
		return objects;
	}
	
	public static void createFile(String fileName) {
		File file = new File(fileName);
		if(!file.exists()) {
			try {
				file.createNewFile();
			} catch (IOException e) {
				logger.error("Error creating file: "+ fileName);
			}
		}
	}
	
	public static void renameFile(String oldName, String newName) {
		File oldFile = new File(oldName);
		File newFile = new File(newName);
		
		if(oldFile.renameTo(newFile)){
			logger.info("Rename succesful from: " + oldName + " to " + newName);
		}else{
			logger.info("Rename failed");
		}
	}
}
