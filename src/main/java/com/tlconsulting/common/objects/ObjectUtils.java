package com.tlconsulting.common.objects;

import java.lang.reflect.Field;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class ObjectUtils {
	
	private static final Logger logger = LogManager.getLogger();
	
	private ObjectUtils() {
	    throw new IllegalStateException("ObjectUtils is an utility class - can't not be initialised");
	}
	
	public static void setPrivateFinalField(Object tobeModifiedObject, String fieldName, Object value) {
		Field field = null;
		try {
			field = tobeModifiedObject.getClass().getDeclaredField(fieldName);
		} catch (NoSuchFieldException | SecurityException e) {
			logger.error("Error when using reflection to get declared field: {}", e);
		}
		field.setAccessible(true);
		try {
			field.set(tobeModifiedObject, value);
		} catch (IllegalArgumentException | IllegalAccessException e) {
			logger.error("Error when using reflection to modify field: {}", e);
		}
	}
}
