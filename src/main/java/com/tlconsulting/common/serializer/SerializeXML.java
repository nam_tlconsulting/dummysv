package com.tlconsulting.common.serializer;

import java.lang.reflect.Type;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import com.fasterxml.jackson.dataformat.xml.XmlMapper;

public class SerializeXML implements SerializeStrategy{

	private XmlMapper xmlMapper;
	private static final Logger logger = LogManager.getLogger();
	
	public SerializeXML() {
		xmlMapper = new XmlMapper();
	}
	
	@Override
	public <T> String serialize(T object) {
		XmlMapper xmlMapper = new XmlMapper();
		String xml = "";
		try {
			xml = xmlMapper.writeValueAsString(object);
		}
		catch (Exception ex) {
			logger.fatal("Serialize: Error in converting object to XML String", ex);
		}
	    
		return xml;
	}

	@Override
	public <T> T deserialize(String content, Class<T> classType) {
		T object = null;
		try {
			object = xmlMapper.readValue(content, classType);
		}
		catch (Exception ex) {
			logger.fatal("Deserialize: Error in parsing XML", ex);
		}
	    
		return object;
	}

	@Override
	public <T> String serialize(T object, Type classType) {
		// TODO Auto-generated method stub
		return null;
	}

}
