package com.tlconsulting.common.serializer;

import java.lang.reflect.Type;

import com.google.gson.ExclusionStrategy;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

public class SerializeJson implements SerializeStrategy {

	private Gson gson;
	
	public SerializeJson() {
		this.gson = new GsonBuilder().setPrettyPrinting().create();
	}
	
	public SerializeJson(ExclusionStrategy exclusionStrategy) {
		this.gson = new GsonBuilder().setExclusionStrategies(exclusionStrategy).setPrettyPrinting().create();
	}
	
	@Override
	public <T> String serialize(T object) {
		String content = gson.toJson(object);
		return content;
	}

	@Override
	public <T> T deserialize(String content, Class<T> classType) {
		T object = gson.fromJson(content, classType);
		return object;
	}

	@Override
	public <T> String serialize(T object, Type classType) {
		String content = gson.toJson(object, classType);
		return content;
	}
}
