package com.tlconsulting.common.serializer;

import java.lang.reflect.Type;

import com.tlconsulting.frameworks.service_virtualisation_framework.EndPointSummaryStrategy;

public abstract class Serializer {
	
	public <T> String toJSON() {
		return new SerializeJson().serialize(this);
	}
	
	public <T> String toXML() {
		return new SerializeXML().serialize(this);
	}

	public static <T> T jsonToObject(String content, Class<T> classType) {
		return new SerializeJson().deserialize(content, classType);
	}
	
	public static <T> T xmlToObject(String content, Class<T> classType) {
		return new SerializeXML().deserialize(content, classType);
	}
	
	public static <T> String toJSON(T object, Type classType) {
		return new SerializeJson().serialize(object, classType);
	}
	
	public static <T> String toJSON(T object, Type classType, EndPointSummaryStrategy strategy) {
		return new SerializeJson(strategy).serialize(object, classType);
	}
}
