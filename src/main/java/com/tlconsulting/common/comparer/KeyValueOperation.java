package com.tlconsulting.common.comparer;

public abstract class KeyValueOperation {
	public String key;
	public String value;
	public MatchOperation operation;
	
	public KeyValueOperation(String key, MatchOperation operation, String value) {
		this.key = key;
		this.value = value;
		this.operation = operation;
	}
}