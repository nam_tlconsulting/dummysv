package com.tlconsulting.common.comparer;

public abstract class ValueOperation {
	public String value;
	public MatchOperation operation;
	
	public ValueOperation(MatchOperation operation, String value) {
		this.value = value;
		this.operation = operation;
	}
}