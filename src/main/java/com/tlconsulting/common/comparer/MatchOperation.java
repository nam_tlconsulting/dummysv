package com.tlconsulting.common.comparer;

public enum MatchOperation {
	contains,
	equalTo,
	equalToXml,
	equalToJson,
	doesNotMatch,
	matches,
	matchesXPath,
	matchesJsonPath
}
