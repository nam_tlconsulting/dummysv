pipeline {
	environment {
		GIT_BRANCH = "${env.BRANCH_NAME.replaceAll(' ', '_')}"
		GIT_URL = "tlconsulting/service-virtualisation-framework.git"
		TAG_NAME = ""
		BRANCH = "${GIT_BRANCH.replaceAll('[/|(|)|!|:|,]', '-')}"
		RELEASE = ""
		TEST_ENVIRONMENT = "e2e"
		SLACK_URL = "https://tlconsultinggroup.slack.com/services/hooks/jenkins-ci/"
		SLACK_CHANNEL = "amp-service-v"
		SLACK_TOKEN = "qefJTJL5PxIk9Pf10a4WlhKs"
		GIT_LAB_KEY = "jenkins-generated-ssh-key"
		COMMIT_MESSAGE = ""
        NEXUS_VERSION = "nexus3"
        NEXUS_PROTOCOL = "http"
        NEXUS_URL = "52.62.51.226:8081"
        NEXUS_REPOSITORY = ""
        NEXUS_CREDENTIAL_ID = "NexusID"
	}
    agent any
    tools {
        maven 'Maven 3.6.1'
        jdk 'jdk8'
    }
	options {
      gitLabConnection('TLConsulting Gitlab')
    }
    triggers {
		gitlab(branchFilterType: 'All', skipWorkInProgressMergeRequest: true, triggerOnPush: true, triggerOnMergeRequest: true, triggerOnNoteRequest: false, ciSkip: false, setBuildDescription: true, addNoteOnMergeRequest: true, addCiMessage: true, addVoteOnMergeRequest: true, acceptMergeRequestOnSuccess: true)
	}
    stages {
        stage ('Build') {
            steps {
            	script {
					echo "CONFIGURE THE ENVIRONMENT"
	                echo "PATH = ${PATH}"
	                echo "M2_HOME = ${M2_HOME}"
					COMMIT_MESSAGE = sh(returnStdout: true, script: 'git log -1 --pretty=%B').trim()
					if (BRANCH == 'master'){
						NEXUS_REPOSITORY = "maven-releases"
						echo "*** BUILDING A MAVEN RELEASE ***"
						sh "mvn versions:set -DremoveSnapshot -DgenerateBackupPoms=false -Denvironment=jenkins"
					}
					else {
						NEXUS_REPOSITORY = "maven-snapshots"
					}
					RELEASE = readMavenPom().getVersion()
					TAG_NAME = "v${RELEASE}-${BUILD_ID}"
					echo "BUILDING BRANCH ${BRANCH} - ${TAG_NAME}"
	                sh "mvn clean dependency:copy-dependencies package -DskipTests -Denvironment=jenkins"
	                props = readProperties  file: 'target/build.properties'
            	}
            }
            post {
				failure {
					updateGitlabCommitStatus name: 'Build', state: 'failed'
				}
                success {
                    updateGitlabCommitStatus name: 'Build', state: 'success'
                }
            }
        }
        stage('Test') {
			steps {
				script {
					echo "TESTING in ${TEST_ENVIRONMENT} environment"
					sh "mvn test"
	                junit 'target/surefire-reports/**/*.xml'
				}
			}
			post {
				failure {
					updateGitlabCommitStatus name: 'Test', state: 'failed'
				}
				success {
					updateGitlabCommitStatus name: 'Test', state: 'success'
				}
			}
		}
		stage('Deploy') {
			when {
				anyOf {
				branch 'master';
				branch 'develop'
				}
			}
			steps {
				script {
					echo "DEPLOY TO NEXUS MAVEN REGISTRY"
	                pom = readMavenPom file: "pom.xml";
	                // Find built artifact under target folder
	                filesByGlob = findFiles(glob: "target/*.${pom.packaging}");
	                // Print some info from the artifact found
	                echo "${filesByGlob[0].name} ${filesByGlob[0].path} ${filesByGlob[0].directory} ${filesByGlob[0].length} ${filesByGlob[0].lastModified}"
	                // Extract the path from the File found
	                artifactPath = filesByGlob[0].path;
	                // Assign to a boolean response verifying If the artifact name exists
	                artifactExists = fileExists artifactPath;
	                if(artifactExists) {
	                    echo "*** File: ${artifactPath}, group: ${pom.groupId}, packaging: ${pom.packaging}, version ${pom.version}";
	                    nexusArtifactUploader(
	                        nexusVersion: NEXUS_VERSION,
	                        protocol: NEXUS_PROTOCOL,
	                        nexusUrl: NEXUS_URL,
	                        groupId: pom.groupId,
	                        version: pom.version,
	                        repository: NEXUS_REPOSITORY,
	                        credentialsId: NEXUS_CREDENTIAL_ID,
	                        artifacts: [
	                            // Artifact generated such as .jar, .ear and .war files.
	                            [artifactId: pom.artifactId,
	                            classifier: '',
	                            file: artifactPath,
	                            type: pom.packaging],
	                            // upload the pom.xml file
	                            [artifactId: pom.artifactId,
	                            classifier: '',
	                            file: "pom.xml",
	                            type: "pom"]
	                        ]
	                    );
    					echo "TAG THE COMMIT IN GIT WITH THE TAG: ${TAG_NAME}"
						sshagent (credentials: ["${GIT_LAB_KEY}"]) {
						sh "git tag -a ${TAG_NAME} -m 'Build ${TAG_NAME}' ${GIT_COMMIT}"
						sh "git remote set-url origin git@gitlab.com:${GIT_URL}"
						sh "git push origin ${TAG_NAME}"
					}
	                    
	                } else {
	                    error "*** File: ${artifactPath}, could not be found";
	                }
				}
			}
			post {
				failure {
					updateGitlabCommitStatus name: 'Deploy', state: 'failed'
				}
				success {
					updateGitlabCommitStatus name: 'Deploy', state: 'success'
				}
			}
		}
    }
	post { 
        always {
			script{
				echo "CLEANING UP"
			}
			deleteDir()
        }
		success {
			script {
				if (BRANCH == 'master' || BRANCH == 'develop'){
					slackSend(color: "good", channel: "${SLACK_CHANNEL}", token: "${SLACK_TOKEN}", baseUrl: "${SLACK_URL}", message: "Hey team, Jenkins has got a new *${BRANCH}* branch build for ServiceV\n*Build Name*: ServiceV:${env.BUILD_ID}\n*Last Commit*: ${COMMIT_MESSAGE}\n(<${env.RUN_DISPLAY_URL}|Go to Jenkins>)")
				}
				echo "GitLab Action is: ${env.gitlabActionType}"
				if("${env.gitlabActionType}" == 'MERGE'){
					if("${env.gitlabTargetBranch}" == 'master'){
						acceptGitLabMR(useMRDescription: true, removeSourceBranch: false)
					}
					else{
						acceptGitLabMR(useMRDescription: true, removeSourceBranch: true)
					}	
				}
			}
		}
		failure {
			script {
				if (BRANCH == 'master' || BRANCH == 'develop'){
					slackSend(color: "danger", channel: "${SLACK_CHANNEL}", token: "${SLACK_TOKEN}", baseUrl: "${SLACK_URL}", message: "Hey team, a *${BRANCH}* branch build has failed ServiceV:${env.BUILD_ID}\n(<${env.RUN_DISPLAY_URL}|Go to Jenkins>)")
				}
				if(GIT_BRANCH.take(1) == '!'){
					addGitLabMRComment(comment: "Pull Request from ${env.gitlabSourceBranch} to ${env.gitlabTargetBranch} build failed.")
				}
			}
		}
    }
}